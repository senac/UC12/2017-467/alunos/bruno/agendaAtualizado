package br.com.senac.servlet;

import br.com.senac.agenda.dao.UsuarioDAO;
import br.com.senac.agenda.model.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest requisicao, HttpServletResponse resposta) throws ServletException, IOException {
        // tipo da resposta

        String usuario = requisicao.getParameter("user");

        String navegador = requisicao.getHeader("User-Agent");

        resposta.setContentType("text/html");
        PrintWriter out = resposta.getWriter();
        out.print("<html>");
        out.print("<body>");
        out.print("<b>Olá," + usuario + "</b><br/>");
        out.print("Seja Bem Vindo ao Mundo dos Servlets.<br/>");
        out.println("Você está usando o navegado:" + navegador);
        out.print("</body>");
        out.print("</html>");

    }

    @Override
    protected void doPost(HttpServletRequest requisicao, HttpServletResponse resposta) throws ServletException, IOException {

        String nome = requisicao.getParameter("usuario");
        String senha = requisicao.getParameter("senha");

        PrintWriter out = resposta.getWriter();

        out.println("<html>");
        out.println("<body>");

        if ((nome != null && senha != null) && (!nome.trim().isEmpty() && !senha.trim().isEmpty())) {

            UsuarioDAO dao = new UsuarioDAO();

            Usuario usuario = dao.getByName(nome);

            if (usuario != null && usuario.getSenha().equals(senha)) {
                
                // caso não exista sessão o container vai criar
                //caso exista ele vai somente devolver o objeto
                HttpSession session = requisicao.getSession();
                session.setAttribute("user", usuario);
                
                
                
                
                
                
                //out.println("Seja Bem Vindo" + usuario.getNome());
                resposta.sendRedirect("principal.jsp");

            } else {
                if (usuario == null) {
                    out.println("Usuário não Autenticado");
                } else {
                    out.println("Falha na autenticação");
                }
            }

            out.println("</html>");
            out.println("</body>");

        } else {
            
            resposta.sendRedirect("login.html");

        }
        /*Usuario usuario = new Usuario(nome, senha);
      PrintWriter out = resposta.getWriter();
      out.println("<html>");
      out.println("<body>");
      
      if(usuario.getNome().equals("Bruno")&& usuario.getSenha().equals("1234")){
          out.println("Seja Bem Vindo" + usuario.getNome());
  
      }else{
        out.println("Falha na autenticação");
      }
        out.println("</body>");
        out.println("</html>");
         */
    }
}

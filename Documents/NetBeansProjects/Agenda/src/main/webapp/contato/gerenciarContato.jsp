<%@page import="br.com.senac.agenda.model.Contato"%>
<jsp:include page="../header.jsp" />

<%Contato contato = (Contato) request.getAttribute("contato");
    String mensagem = (String) request.getAttribute("mensagem");
    String erro = (String) request.getAttribute("erro");
%>

<% if (mensagem != null) {%>
<div class="alert alert-success" role="alert">
    <%= mensagem%>
</div>
<%}%>

<% if (erro != null) {%>
<div class="alert alert-danger" role="alert">
    <%= erro%>
</div>
<%}%>



<form action="./SalvaContatoServlet" method="post">
    <input type="hidden" name="id" value="<%= contato != null ? contato.getId() : ""%>" />
    <div class="row">
        <div class="col-2">
            <div class="form-group">
                <label for="codigo">C�digo</label>
                <input type="text" class="form-control" id="codigo" readonly name="codigo" readonly value=" <%= contato != null ? contato.getId() : "" %>">
            </div>
        </div>
    </div>


    <div class="row">

        <div class="col-12">

            <div class="form-group">
                <label for="nome">Nome</label>
                <input type="text" class="form-control" id="nome" name="nome" placeholder="Digite seu Nome...">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-4">

            <div class="form-group">
                <label for="telefone">Telefone</label>
                <input type="text" class="form-control" id="telefone" name="telefone" placeholder="Insira seu n�mero...">
            </div>
        </div>

        <div class="col-4">
            <div class="form-group">
                <label for="celular">Celular</label>
                <input type="text" class="form-control" id="celular" name="celular" placeholder="Insira seu n�mero...">

            </div>
        </div>

        <div class="col-4">

            <div class="form-group">
                <label for="fax">FAX</label>
                <input type="text" class="form-control" id="fax" name="fax" placeholder="Insira seu fax...">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-4">

            <div class="form-group">
                <label for="cep">CEP</label>
                <input type="text" class="form-control" id="cep" name="cep" placeholder="Insira seu CEP...">
            </div>
        </div>

        <div class="col-6">
            <div class="form-group">
                <label for="endereco">Endere�o</label>
                <input type="text" class="form-control" id="endereco" name="endereco" placeholder="Insira seu endere�o...">

            </div>
        </div>

        <div class="col-2">

            <div class="form-group">
                <label for="numero">N�mero</label>
                <input type="text" class="form-control" id="numero" name="numero" placeholder="N�">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-3">

            <div class="form-group">
                <label for="bairro">Bairro</label>
                <input type="text" class="form-control" id="cep" name="bairro" placeholder=" ">
            </div>
        </div>

        <div class="col-7">
            <div class="form-group">
                <label for="cidade">Cidade</label>
                <input type="text" class="form-control" id="cidade" name="cidade" placeholder=" ">

            </div>
        </div>

        <div class="col-2">

            <div class="form-group">

                <label for="uf">UF (Estado)</label>

                <select name="uf" class="custom-select">
                    <option selected>Selec. Estado</option>
                    <option value="es">ES</option>
                    <option value="mg">MG</option>
                    <option value="sp">SP</option>
                    <option value="rj">RJ</option>
                </select>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-12">

            <div class="form-group">
                <label for="email">E-mail</label>
                <input type="text" class="form-control" id="email" name="email" placeholder=" ">
            </div>
        </div>
    </div>

    <center><button type="submit" class="btn btn-primary">Salvar</button>
        <button type="reset" class="btn btn-outline-danger">Cancelar</button>
    </center>

</form>


<jsp:include page="../footer.jsp" />
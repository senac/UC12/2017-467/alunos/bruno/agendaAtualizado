<%@page import="java.util.List"%>
<%@page import="br.com.senac.agenda.model.Contato"%>
<jsp:include page="../header.jsp" />

<% List<Contato> lista = (List) request.getAttribute("lista");%>
<% String mensagem = (String) request.getAttribute("mensagem");%>

<center><div><h2>Pesquisa de Contatos</h2></div></center>


<form action="./PesquisarContatoServlet" method="post">
    <div class="row">
        <div class="col-2">

            <div class="form-group">
                <label for="codigo">C�digo</label>
                <input type="text" class="form-control" id="codigo" name="telefone" placeholder=" ">
            </div>
        </div>

        <div class="col-6">
            <div class="form-group">
                <label for="nome">Nome</label>
                <input type="text" class="form-control" id="nome" name="celular" placeholder=" ">

            </div>
        </div>

        <div class="col-2">

            <div class="form-group">
                <label for="uf">Estado</label>
                <select class="custom-select">
                    <option selected>Selec. Estado</option>
                    <option value="1">ES</option>
                    <option value="2">MG</option>
                    <option value="3">SP</option>
                    <option value="4">RJ</option>
                </select>
            </div>

        </div>


        <div class="form-group">
            <button type="submit" class="btn btn-primary " style="padding-top: 30px ">Buscar</button>
        </div>
    </div>
</form>

<table class="table">
    <thead>
        <tr>
            <th>C�digo</th>
            <th>Nome</th>
            <th>Endere�o</th>
            <th>N�mero(TEL)</th>
            <th>E-mail</th>
        </tr>
    </thead>

    <tr>

        <% if (lista != null && lista.size() > 0) {

                for (Contato c : lista) {
        %>
    <tr>
        
        <td><%= c.getId()%></td>
        <td><%= c.getNome()%></td>
        <td><%= c.getEndereco()%></td>
        <td><%= c.getEmail()%></td>

        <td> <a href='./CadastraUsuarioServlet?id=<%= c.getId()%>' > 
                <img src="../resources/imagens/Users-Add-User-icon.png"/></a>
        </td>


        <td>    <a href="" data-toggle="modal" data-target="#Exclusao" 
                   onclick="excluir(<%= c.getId()%>, '<%= c.getNome()%>');">
                <img src="../resources/imagens/Button-Delete-icon.png"/></a>
        </td>


    </tr>

    <% }
      } else %>

    <div class="modal fade" id="Exclusao" tabindex="-1" role="dialog" 
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Excluir</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Deseja realmente apagar este contato <span id="nomeUsuario"></span>?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">N�O</button>

                    <a  id="btnConfirmar" class="btn btn-primary">SIM</a>
                </div>
            </div>
        </div>
    </div>

</table>

<jsp:include page="../footer.jsp" />
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.academica.dao;

import br.com.senac.academica.model.Aluno;
import br.com.senac.academica.model.Curso;
import br.com.senac.academica.model.Professor;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {

    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("AcademicoPU");

    public static EntityManager getEntityManager() {

        try {
            return emf.createEntityManager();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException("Erro ao acessar banco de dados");
        }

    }

    public static void main(String[] args) {
        
       CursoDAO dao = new CursoDAO();
        
       Curso curso = new Curso();
       
       curso.setNome("Bruno");
       
       dao.save(curso);
       
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.academica.bean;

import br.com.senac.academica.dao.AlunoDAO;
import br.com.senac.academica.model.Aluno;
import java.io.Serializable;
import javax.faces.view.ViewScoped;

import javax.inject.Named;

/**
 *
 * @author Sala302b
 */

@Named (value = "alunoBean")
@ViewScoped
public class AlunoBean implements Serializable{
    
    private Aluno aluno;
    private AlunoDAO dao;

    public AlunoBean() {
        
        this.aluno = new Aluno();
        this.dao = new AlunoDAO();
    }
    
    public void salvar(){
        
        if(this.aluno.getId() == 0){
       dao.save(aluno);
    }else {
            dao.update(aluno);
        }
    
    }
    
    public void novo(){
        this.aluno = new Aluno();
    }
    
    public Aluno getAluno() {
        return aluno;
    }

    public void setAluno(Aluno aluno) {
        this.aluno = aluno;
    }
    
}

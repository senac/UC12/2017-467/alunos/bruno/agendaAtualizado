/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.academica.bean;



import br.com.senac.academica.dao.AlunoDAO;
import br.com.senac.academica.model.Aluno;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

@Named(value = "pesquisaAlunoBean")
@ViewScoped
public class PesquisaAlunoBean extends Bean {

    private Aluno alunoSelecionado;
    private List<Aluno> lista;
    private AlunoDAO dao;

    private String codigo;
    private String nome;

    public PesquisaAlunoBean() {
    }

    @PostConstruct
    public void init() {
        try {
            dao = new AlunoDAO();
            alunoSelecionado = new Aluno();
            lista = dao.findAll();

        } catch (Exception ex) {
            ex.printStackTrace();
            this.addMessageErro("Falha ao carregar itens.");
        }
    }

    public void pesquisa() {
        try {
            this.lista = this.dao.findByFiltro(codigo, nome);

        }catch(Exception ex){
            ex.printStackTrace();
            
        }
    }
    
    public void salvar (){
     if(this.alunoSelecionado.getId() == 0){
       dao.save(alunoSelecionado);
    }else {
            dao.update(alunoSelecionado);
        }
        
    }
    
    
    

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Aluno getAlunoSelecionado() {
        return alunoSelecionado;
    }

    public void setAlunoSelecionado(Aluno alunoSelecionado) {
        this.alunoSelecionado = alunoSelecionado;
    }

    public List<Aluno> getLista() {
        return lista;
    }

    public void setLista(List<Aluno> lista) {
        this.lista = lista;
    }
}



package br.com.senac.academica.bean;

import br.com.senac.academica.dao.AlunoDAO;
import br.com.senac.academica.dao.ProfessorDAO;
import br.com.senac.academica.model.Aluno;
import br.com.senac.academica.model.Professor;
import java.io.Serializable;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

@Named (value = "professorBean")
@ViewScoped
public class ProfessorBean implements Serializable{
    
    private Professor professor;
    private ProfessorDAO dao;

    public ProfessorBean() {
        this.professor= new Professor();
        this.dao = new ProfessorDAO();
    }

    public void salvar1(){
        
        if(this.professor.getId() == 0){
       dao.save(professor);
    }else {
            dao.update(professor);
        }
    
    }
    
      public void novo(){
        this.professor = new Professor();
    }
    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }
     
}


    
  